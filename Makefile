#************************************************************************
#
# Copyright (c) 2010 Kohei Yoshida
# 
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
#***********************************************************************

EXEC=orcus-grid-core

OBJDIR=./obj
SRCDIR=./source
INCDIR=./include

MDDSDIR=../multidimalgorithm

CPPFLAGS= \
	-I$(INCDIR) -I$(MDDSDIR)/inc \
	`pkg-config --cflags gtkmm-2.4` \
	-D_REENTRANT -O2 -g -Wall -std=c++0x

LDFLAGS=`pkg-config --libs gtkmm-2.4`

HEADERS= \
	$(INCDIR)/orcus/grid/grid.hpp \
	$(INCDIR)/orcus/grid/top_window.hpp \
	$(INCDIR)/orcus/grid/grid_control.hpp

OBJFILES= \
	$(OBJDIR)/grid/grid.o \
	$(OBJDIR)/grid/top_window.o \
	$(OBJDIR)/grid/grid_control.o \
	$(OBJDIR)/main.o

DEPENDS= \
	$(HEADERS)

all: $(EXEC)

pre:
	mkdir -p $(OBJDIR)/grid 2>/dev/null || /bin/true

$(OBJDIR)/grid/grid_control.o: $(SRCDIR)/grid/grid_control.cpp $(DEPENDS)
	$(CXX) $(CPPFLAGS) -c -o $@ $(SRCDIR)/grid/grid_control.cpp

$(OBJDIR)/grid/top_window.o: $(SRCDIR)/grid/top_window.cpp $(DEPENDS)
	$(CXX) $(CPPFLAGS) -c -o $@ $(SRCDIR)/grid/top_window.cpp

$(OBJDIR)/grid/grid.o: $(SRCDIR)/grid/grid.cpp $(DEPENDS)
	$(CXX) $(CPPFLAGS) -c -o $@ $(SRCDIR)/grid/grid.cpp

$(OBJDIR)/main.o: $(SRCDIR)/main.cpp $(DEPENDS)
	$(CXX) $(CPPFLAGS) -c -o $@ $(SRCDIR)/main.cpp

$(EXEC): pre $(OBJFILES)
	$(CXX) $(LDFLAGS) $(OBJFILES) -o $(EXEC)

run: $(EXEC)
	./$(EXEC)

clean:
	rm -rf $(OBJDIR)
	rm $(EXEC)

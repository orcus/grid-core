/*************************************************************************
 *
 * Copyright (c) 2010 Kohei Yoshida
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include "orcus/grid/grid_control.hpp"

#include <iostream>
#include <functional>

using namespace std;

namespace orcus { namespace grid {

grid_control::grid_control()
{
}

grid_control::~grid_control()
{
}

namespace {

struct point
{
    double x;
    double y;

    point(double _x, double _y) : x(_x), y(_y) {}
};

struct rect
{
    double x1;
    double y1;
    double x2;
    double y2;

    rect(double _x1, double _y1, double _x2, double _y2) :
        x1(_x1), y1(_y1), x2(_x2), y2(_y2) {}
};

class point_drawer : public unary_function<pair<grid_control::point_store_type::point, size_t>, void>
{
public:
    point_drawer(Cairo::RefPtr<Cairo::Context>& cr) : m_cr(cr) {}

    void draw_circle (const point& pt)
    {
        m_cr->arc(pt.x, pt.y, 3, 0, 2.0*M_PI);
        m_cr->fill();
    }

    void draw_line(double x1, double y1, double x2, double y2)
    {
        m_cr->save();
        m_cr->set_source_rgba(0, 0.2, 0.8, 1);
        m_cr->move_to(x1, y1);
        m_cr->line_to(x2, y2);
        m_cr->stroke();
        m_cr->restore();
    }

    void draw_border(double x1, double y1, double x2, double y2)
    {
        m_cr->save();
        m_cr->set_source_rgba(0.8, 0.8, 0.8, 1);
        m_cr->move_to(x1, y1);
        m_cr->line_to(x2, y2);
        m_cr->stroke();
        m_cr->restore();
    }

private:
    Cairo::RefPtr<Cairo::Context>& m_cr;
};

void walk_tree_and_draw(point parent, const rect& bound,
                        const grid_control::point_store_type::node_access& node, point_drawer& drawer)
{
    if (!node)
        return;

    drawer.draw_border(bound.x1, node.y(), bound.x2, node.y());
    drawer.draw_border(node.x(), bound.y1, node.x(), bound.y2);

    drawer.draw_line(parent.x, parent.y, node.x(), node.y());

    parent.x = node.x();
    parent.y = node.y();

    drawer.draw_circle(parent);

    walk_tree_and_draw(parent, rect(parent.x, bound.y1, bound.x2, parent.y), node.northeast(), drawer);
    walk_tree_and_draw(parent, rect(bound.x1, bound.y1, parent.x, parent.y), node.northwest(), drawer);
    walk_tree_and_draw(parent, rect(parent.x, parent.y, bound.x2, bound.y2), node.southeast(), drawer);
    walk_tree_and_draw(parent, rect(bound.x1, parent.y, parent.x, bound.y2), node.southwest(), drawer);
}

void walk_quad_tree(const grid_control::point_store_type::node_access& node, point_drawer& drawer, double width, double height)
{
    if (!node)
        return;

    double x = node.x();
    double y = node.y();

    drawer.draw_border(0, y, width, y);
    drawer.draw_border(x, 0, x, height);

    point parent(x, y);
    drawer.draw_circle(parent);

    walk_tree_and_draw(parent, rect(x, 0, width, y), node.northeast(), drawer);
    walk_tree_and_draw(parent, rect(0, 0, x, y), node.northwest(), drawer);
    walk_tree_and_draw(parent, rect(x, y, width, height), node.southeast(), drawer);
    walk_tree_and_draw(parent, rect(0, y, x, height), node.southwest(), drawer);
}

}

bool grid_control::on_expose_event(GdkEventExpose* event)
{
    Glib::RefPtr<Gdk::Window> window = get_window();
    if (!window)
        return true;

    Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();

    if (event)
    {
        // clip to the area indicated by the expose event so that we only
        // redraw the portion of the window that needs to be redrawn
        cr->rectangle(event->area.x, event->area.y,
                      event->area.width, event->area.height);
        cr->clip();
    }

    // paint the background.
    cr->save();
    cr->set_source_rgba(1, 1, 1, 1);   // white
    cr->paint();
    cr->restore();

    // draw the outer borders.
    cr->save();
    cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
    cr->set_line_width(1);
    cr->set_source_rgba(0, 0, 0, 1);   // white
    cr->rectangle(0, 0, width, height);
    cr->stroke();
    cr->restore();

    // draw circles.
    cr->save();
    cr->set_source_rgba(0.5, 0, 0, 1);
    cr->set_line_width(1);

    point_drawer drawer(cr);
    walk_quad_tree(m_points.get_node_access(), drawer, width, height);

    cr->restore();

    return true;
}

bool grid_control::on_button_press_event(GdkEventButton* event)
{
    m_points.insert(event->x, event->y, 1);
    Glib::RefPtr<Gdk::Window> win = get_window();
    if (win)
    {
        Gtk::Allocation allocation = get_allocation();
        const int width = allocation.get_width();
        const int height = allocation.get_height();
        Gdk::Rectangle r(0, 0, width, height);
        win->invalidate_rect(r, false);
    }

    return true;
}

bool grid_control::on_button_release_event(GdkEventButton* /*event*/)
{
    return true;
}

}}
